/*
 * Universidad del Valle de Guatemala
 * Administraci�n de la Informaci�n
 * Donald Vel�squez - doanvelagui
 * abril - mayo de 2012
 */
 
/*
 * Example 3
 * Using insert.
 * Assumption: there is no databases created.
 * Recommendation: use verbose mode to see all messages.
 */
 
-- creating database and using
show databases;
create database prueba;
use database prueba;
show databases;

-- crating tabla1, same of ej2.sql
create table tabla1(
	col1	int,
	col2	float,
	col3	date,
	col4	char(10),
	Primary key pk_tabla1 ( col1 ),
	check	chk_col2 (col2 > 10)
);
show tables;




-- inserting values
insert into tabla1 values
	(1,	1.0,	2001-05-12,	"mi dato");
show tables;

-- primary key again.
insert into tabla1 values
	(1,	1.0,	2001-05-12,	"mi dato");
show tables;

-- inserting values with different primary key
insert into tabla1 values
	(2,	1.0,	2001-05-12,	"mi dato");
show tables;

-- inserting incomplete tupla
insert into tabla1 values
	(2,	1.0,	2001-05-12);
show tables;

-- inserting invalid kind of column
insert into tabla1 values
	("2",	1.0,	2001-05-12,	"mi dato");
show tables;



-- inserting values in specific columns. 
insert into tabla1 (col1, col3) values
	(3,	2001-05-12);
show tables;

-- inserting values with a repeated value of primary key 
insert into tabla1 (col1, col4) values
	(3,	"hola");
show tables;

-- inserting with different values of columns and tuplas 
insert into tabla1 (col1, col3) values
	(3,	2001-05-12, 1.2);
show tables; 

-- inserting with different values of columns and tuplas 
insert into tabla1 (col2, col3, col1) values
	(3.0,	2001-05-12, 15);
show tables;

-- drop database remove all tables and contents.
drop database prueba;