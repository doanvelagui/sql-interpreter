/*
 * Universidad del Valle de Guatemala
 * Administraci�n de la Informaci�n
 * Donald Vel�squez - doanvelagui
 * abril - mayo de 2012
 */
 
/*
 * Example 4
 * Alter table
 * Assumption: there is no databases created.
 * Recommendation: use verbose mode to see all messages.
 */

-- show databases stored
show databases;
create database data1;
show databases;
use database data1;

--dddd
create table tabla1(
	columna1	int,
	columna2	int,
	primary key pk_tb1 (columna1)
);

alter table tabla1(
	add column columna3 int
);

drop database data1;