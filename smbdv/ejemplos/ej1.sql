/*
 * Universidad del Valle de Guatemala
 * Administraci�n de la Informaci�n
 * Donald Vel�squez - doanvelagui
 * abril - mayo de 2012
 */
 
/*
 * Example 1
 * All options related to database.
 * Assumption: there is no databases created.
 * Recommendation: use verbose mode to see all messages.
 */

-- show databases stored
show databases;

-- should be added data_2
create database data_2;
show databases;

-- should be renamed
alter database data_2 rename to data_1;
show databases;

-- error, data_1 exists
create database data_1;
show databases;

-- create data_2
create database data_2;
show databases;

-- error, data_3 don't exists
alter database data_3 rename to data_1;

-- error, data_1 exists
alter database data_2 rename to data_1;
show databases;

-- should be renamed
alter database data_2 rename to data_0;
show databases;

-- should be deleted
drop database data_0;
show databases;

-- error, data_0 don't exists
drop database data_0;
show databases;

-- error, data_0 don't exists
use database data_0;

-- use database data_1
use database data_1;

-- drop database data_1, it is in use, but don't return error when is deleted.
-- this is managed when other operation (DDL or DML) is executed and requires a database in use.
drop database data_1;
show databases;