/*
 * Universidad del Valle de Guatemala
 * Administraci�n de la Informaci�n
 * Donald Vel�squez - doanvelagui
 * abril - mayo de 2012
 */
 
/*
 * Example 2
 * Using create table, rename table, drop table.
 * Assumption: there is no databases created.
 * Recommendation: use verbose mode to see all messages.
 */


-- show databases stored
show databases;

-- should be added myBD, if not exits.
create database myBD;
show databases;

-- error because no database selected
show tables;

-- error because no database selected, can't know if table exists...
show columns from tabla_dummy;

-- use database myBD
use database myBD;

-- create tabla1
	--with 4 columns (4 types: integer, float, date and char(n))
	--with col1 as primary key
	--without foreign key(s)
	--with check in col2 > 10
create table tabla1(
	col1	int,
	col2	float,
	col3	date,
	col4	char(10),
	Primary key pk_tabla1 ( col1 ),
	check	chk_col2 (col2 > 10)
);

-- display tables
show tables;

-- display columns
show columns from tabla1;

-- it is possible because this don't have any reference or foreign key.
alter table tabla1 rename to tabla0;
show tables;


-- create tabla2
create table tabla2(
	col1	int,
	col2	int,
	Primary key pk_tabla ( col3 ) --error col3 don't exists
);
show tables;

-- create tabla2
create table tabla2(
	col1	int,
	col2	int,
	Primary key pk_tabla ( col2 ),
	Foreign key fk_tabla1 ( col2 ) references tabla1 --error tabla1 don't exist (renamed to tabla0)
);
show tables;

-- create tabla2
create table tabla2(
	col1	int,
	col2	int,
	col3	date,
	Primary key pk_tabla ( col2 ),
	Foreign key fk_tabla1 ( col1 ) references tabla0,
	check	chk_col1 (col1 <> 0) -- see grammar for detail
);
show tables;

-- alter table whit foreign key(s) isn't possible.
alter table tabla2 rename to tabla1;
show tables;


-- alter table referenced by foreign key(s) isn't possible
alter table tabla0 rename to tabla1;
show tables;

-- drop database remove all tables, don't import references and foreign keys
drop database myBD;