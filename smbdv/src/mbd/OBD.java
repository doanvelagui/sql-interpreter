/*
 * Universidad del Valle de Guatemala
 * Administraci�n de la Informaci�n
 * 
 * Donald Vel�squez
 * carn�: 09379
 * abril - mayo de 2012
 */

package mbd;

import java.io.*;
import java.util.*;

import db.*;
import db.Tabla.*;

public class OBD {
	
	// VARIABLES
	
	/** main **/
	protected MBD mbd;
	
	/** All databases **/
	protected ABD all;
	
	/** database in use **/
	protected String BDU = "";
	
	
	
	
	
	// ALL DATABASES
	
	/**
	 * load all databases
	 * @param path path of common storage
	 */
	public OBD(MBD mbd){
		this.mbd = mbd;
		try{
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(mbd.path+"/meta.data"));
			all = (ABD) in.readObject();
			in.close();
		}
		catch(Exception e){
			all = new ABD();
		}
	}
	
	/**
	 * Routine to save the meta-data
	 * @return true if success
	 */
	public boolean saveMetaData(){
		try{
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(mbd.path+"/meta.data"));
			out.writeObject(all);
			out.close();
			return true;
		}catch (Exception e) {
			System.out.println("\nFATAL ERROR: No se guardo metadata\n\t"+e.getMessage());
			return false;
		}
	}
	
	/**
	 * Routine to save meta-data of a database; 
	 * @param id identifier of database
	 * @param bd database to save
	 * @return true if success
	 */
	public boolean saveMetaData(String id, BD bd){
		try{
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(mbd.path+"/"+id+"/meta.data"));
			out.writeObject(bd);
			out.close();
			return true;
		}catch (Exception e) {
			System.out.println("\nFATAL ERROR: No se guardo metadata "+id+".\n\t"+e.getMessage());
			return false;
		}
	}
	
	/**
	 * Routine to save a table; 
	 * @param id identifier of table
	 * @param tabla table to save
	 * @return true if success
	 */
	public boolean saveTable(String id, Tabla tabla){
		try{
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(mbd.path+"/"+BDU+"/"+id));
			out.writeObject(tabla);
			out.close();
			return true;
		}catch (Exception e) {
			System.out.println("\nFATAL ERROR: No se guardo tabla "+id+".\n\t"+e.getMessage());
			return false;
		}
	}
	
	/**
	 * Returns a object saved by serialization.
	 * @param path where are saved
	 * @return object deserializated
	 */
	public Object readObject(String path){
		Object o = null;
		try{
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(path));
			o = in.readObject();
			in.close();
		} catch (Exception e) {
			System.out.println("\nFATAL ERROR: Leyendo archivo\n\t"+path+".\n\t"+e.getMessage());
		}
		return o;
	}
	
	
	
	
	
	// DATABASE
	
	/**
	 * Create a database 
	 * @param id of database
	 * @return true if success
	 */
	public boolean createDatabase(String id){
		boolean success = false;
		if(!all.lbd.contains(id)){
			File file = new File(mbd.path+"/"+id);
			BD bd = new BD();
			try{
				if (file.mkdir()){
					boolean pass = false;
					while (!pass)
						pass = saveMetaData(id, bd);
					pass = false;
					while (!pass)
						pass = all.addBD(id);
					pass = false;
					while (!pass)
						pass = saveMetaData();
					success = true;
				}else System.out.println("\nERROR: CREATE DATABASE.\n\tNo se pudo crear base de datos "+id+".");
			}catch (Exception e) { System.out.println("\nERROR FATAL: CREATE DATABASE.\n\t"+e.getMessage()); }
		}else System.out.println("\nERROR: CREATE DATABASE.\n\tBase de datos "+id+" ya existe.");
		return success;
	}
	
	/**
	 * Drop a database
	 * @param id identifier of database
	 * @return true if success
	 */
	public boolean dropDatabase(String id){
		boolean success = false;
		if(all.lbd.contains(id)){
			File file = new File(mbd.path+"/"+id);
			try{
				boolean pass = false;
				while (!pass)
					pass = all.removeBD(id);
				pass = false;
				while (!pass)
					pass = saveMetaData();
				pass = false;
				while (!pass){
					File[] database = file.listFiles();
					for (int n=0; n<database.length; n++)
						database[n].delete();
					pass = file.delete();
				}
				success = true;
			}catch (Exception e) { System.out.println("\nERROR FATAL: DROP DATABASE "+id+"\n\t"+e.getMessage()); }
		}else System.out.println("\nERROR: DROP DATABASE.\n\tBase de datos "+id+" no existe.");
		return success;
	}
	
	/**
	 * Rename a database
	 * @param id identifier of database
	 * @param newid new identifier of database
	 * @return true if success
	 */
	public boolean alterDatabase(String id, String newid){
		boolean success = false;
		if(all.lbd.contains(id)){
			if(!all.lbd.contains(newid)){ 
				File file = new File(mbd.path+"/"+id);
				File file2 = new File(mbd.path+"/"+newid);
				try{
					boolean pass = false;
					while (!pass)
						pass = all.setBD(id, newid);
					pass = false;
					while (!pass)
						pass = saveMetaData();
					pass = false;
					while (!pass)
						pass = file.renameTo(file2);
					success = true;
				}catch (Exception e) { System.out.println("\nERROR FATAL: RENAME DATABASE.\n\t"+e.getMessage()); }
			}else System.out.println("\nERROR: RENAME DATABASE.\n\tBase de datos "+newid+" ya existe.");
		}else System.out.println("\nERROR: RENAME DATABASE.\n\tBase de datos "+id+" no existe.");
		return success;
	}
	
	/**
	 * Show all databases stored and number of tables in each database
	 */
	public void showDatabase(){
		if(!all.lbd.isEmpty()) {
			System.out.println("\nLista de bases de datos almacenadas.");
			for (int n=0;n<all.lbd.size();n++){
				BD bd = (BD) readObject(mbd.path+"/"+all.lbd.get(n)+"/meta.data");
				if (bd!=null)
					System.out.println("\t- "+all.lbd.get(n)+": "+bd.tablas.size()+ " tabla(s).");
			}
		}else System.out.println("\nNo hay bases de datos.");
	}
	
	/**
	 * Select a database to use
	 * @param id name of database
	 * @return true if success
	 */
	public boolean useDatabase(String id){
		boolean success = false;
		if(all.lbd.contains(id)){
			BDU = id;
			success = true;
		}else System.out.println("\nERROR: USE DATABASE.\n\tNo existe la base de datos "+id+".");
		return success;
	}
	
	
	
	
	
	// TABLE
	/**
	 * Create table
	 * @param id identifier of table
	 * @param columnas list of columns
	 * @param tipos list of types of columns
	 * @param idpk identifier of primary key
	 * @param pk column of primary key
	 * @param idfk list of identifiers of foreign keys
	 * @param cfk columns of foreign keys
	 * @param tfk tables referenced by foreign keys
	 * @return true if success.
	 */
	public boolean createTable(String id, LinkedList<String> columnas, LinkedList<Integer> tipos, String idpk, String pk,
			LinkedList<String> idfk, LinkedList<String> cfk, LinkedList<String> tfk, LinkedList<String> idchk,
			LinkedList<String> colchk, LinkedList<Integer> relchk, LinkedList<String> valchk, LinkedList<Integer> tipchk){
		boolean success = false;
		String ErrMsg = "\nERROR: CREATE TABLE: "+id+".\n\t";
		if(all.lbd.contains(BDU)){
			BD bd = (BD) readObject(mbd.path+"/"+BDU+"/meta.data");
			if (!bd.tablas.contains(id)) {
				Tabla tabla = new Tabla();
				
				int valCol = 0;
				for (int n=0; n<columnas.size(); n++) {
					if (!tabla.columnas.contains(columnas.get(n))) {
						if (tabla.addColumn(columnas.get(n), tipos.get(n))) valCol++;
						else System.out.println(ErrMsg+"Columna "+columnas.get(n)+" No se creo.");
					} else System.out.println(ErrMsg+"Nombre de columna duplicado: "+columnas.get(n));
				}
				
				boolean valPK = false;
				if (!idpk.equals("")){
					if(tabla.columnas.contains(pk))
						if(tabla.addPrimaryKey(pk, idpk)) valPK = true;
						else System.out.println(ErrMsg+"No se pudo crear Primary Key "+idpk+".");
					else System.out.println(ErrMsg+"Primary Key: Columna "+pk+" no definida.");
				}else System.out.println(ErrMsg+"No hay primary key indicado.");
				
				int valCHK = 0;
				for (int n=0; n<idchk.size(); n++){
					if(tabla.columnas.contains(colchk.get(n)))
						if(!tabla.checks.contains(idchk.get(n))){
							Object ob = null;
							switch (tipchk.get(n)){
							case Tabla.INT:		ob = Integer.parseInt(valchk.get(n)); break;
							case Tabla.FLOAT:	ob = Float.parseFloat(valchk.get(n)); break;
							}
							if(tabla.addCheck(colchk.get(n), idchk.get(n), relchk.get(n), ob)) valCHK++;
							else System.out.println(ErrMsg+"CHECK: check "+idchk.get(n)+" no pudo ser creado.");
						}else System.out.println(ErrMsg+"CHECK: check "+idchk.get(n)+" ya existe.");
					else System.out.println(ErrMsg+"CHECK: columna "+colchk.get(n)+" no existe.");
				}
				
				int valFK = 0;
				for (int n=0; n<idfk.size(); n++){
					if(tabla.columnas.contains(cfk.get(n)))
						if(bd.tablas.contains(tfk.get(n)))
							if(!id.equals(tfk.get(n)))
								if(tabla.addForeignKey(cfk.get(n), tfk.get(n), idfk.get(n))) valFK++;
								else System.out.println(ErrMsg+"Foreign Key: Llave foranea "+idfk.get(n)+" ya existe.");
							else System.out.println(ErrMsg+"Foreign Key: Tabla creada es la tabla referenciada.");
						else System.out.println(ErrMsg+"Foreign Key: Tabla "+tfk.get(n)+" no existe.");
					else System.out.println(ErrMsg+"Foreign Key: Columna "+cfk.get(n)+" no existe.");
				}
				
				if (valCol==columnas.size() && valPK && valFK==idfk.size() && valCHK==idchk.size()){
					boolean crefs = true; 
					LinkedList<Tabla> refs = new LinkedList<Tabla>();
					LinkedList<String> tbs = new LinkedList<String>();
					
					
					String[] keys = new String[tabla.foreingKeys.size()]; 
					keys = tabla.foreingKeys.keySet().toArray(keys);
					for (int n=0; n<keys.length; n++){
						FKR fk = tabla.foreingKeys.get(keys[n]);
						Tabla ref;
						if(tbs.contains(fk.table)){
							int index = tbs.indexOf(fk.table);
							ref = refs.get(index);
						}else{
							ref = (Tabla)readObject(mbd.path+"/"+BDU+"/"+fk.table);
							refs.add(ref);
							tbs.add(fk.table);
						}
						if (!ref.addReference(id, keys[n])) {
							System.out.println(ErrMsg+"No se pudo crear referencia en tabla "+fk.table);
							crefs = false;
						}
					}
					
					if(crefs){
						success = true;
						for(int n=0; n<refs.size(); n++){
							if(!saveTable(tbs.get(n), refs.get(n))){
								System.out.println(ErrMsg+"No se pudo guardar tabla referenciada "+tbs.get(n));
								success = false;
							}
						}if(!saveTable(id, tabla)){
							System.out.println(ErrMsg+"No se pudo guardar tabla creada "+id);
							success = false;
						}
						if(!bd.addTable(id)){
							System.out.println(ErrMsg+"No se pudo guardar tabla creada "+id+" en base de dato "+BDU);
							success = false;
						}
						if(!saveMetaData(BDU, bd)){
							System.out.println(ErrMsg+"No se pudo guardar base de dato "+BDU);
							success = false;
						}
					}
				}
			}else System.out.println(ErrMsg+"Tabla "+id+" ya existe.");
		}else System.out.println("\nERROR: CREATE TABLE "+id+".\n\tNo hay base de datos seleccionada o ha sido eliminada.");
		return success;
	}
	
	/**
	 * Delete a table
	 * @param id table to delete
	 * @return true if success
	 */
	public boolean dropTable(String id){
		boolean success = false;
		String ErrMsg = "\nERROR: DROP TABLE: "+id+".\n\t";
		
		if(all.lbd.contains(BDU)){
			BD bd = (BD) readObject(mbd.path+"/"+BDU+"/meta.data");
			if (bd.tablas.contains(id)){
				Tabla tabla = (Tabla) readObject(mbd.path+"/"+BDU+"/"+id);
				if(tabla.references.isEmpty())
					if(tabla.foreingKeys.isEmpty()){
						File file = new File(mbd.path+"/"+BDU+"/"+id);
						success = file.delete();
						bd.removeTable(id);
						saveMetaData(BDU, bd);
					}else System.out.println(ErrMsg+"Esta tabla tiene referencias a otras tablas.");
				else System.out.println(ErrMsg+"Hay otras tablas referenciando esta tabla.");
			}else System.out.println(ErrMsg+"La tabla "+id+" no existe.");
		}else System.out.println(ErrMsg+"No hay base de datos seleccionada o ha sido eliminada.");
		return success;
	}
	
	/**
	 * Rename a table
	 * @param id identifier of table
	 * @param newid new identifier of table
	 * @return true if success
	 */
	public boolean alterTable(String id, String newid){
		boolean success = false;
		String ErrMsg = "\nERROR: RENAME TABLE: "+id+" TO "+newid+".\n\t";
		
		if(all.lbd.contains(BDU)){
			BD bd = (BD) readObject(mbd.path+"/"+BDU+"/meta.data");
			if (bd.tablas.contains(id)){
				if (!bd.tablas.contains(newid)){
					Tabla tabla = (Tabla) readObject(mbd.path+"/"+BDU+"/"+id);
					if(tabla.references.isEmpty())
						if(tabla.foreingKeys.isEmpty()){
							File file = new File(mbd.path+"/"+BDU+"/"+id);
							File file2 = new File(mbd.path+"/"+BDU+"/"+newid);
							success = file.renameTo(file2);
							bd.setTable(id, newid);
							saveMetaData(BDU, bd);
						}else System.out.println(ErrMsg+"Esta tabla tiene referencias a otras tablas.");
					else System.out.println(ErrMsg+"Hay otras tablas referenciando esta tabla.");
				}else System.out.println(ErrMsg+"La tabla "+newid+" ya existe.");
			}else System.out.println(ErrMsg+"La tabla "+id+" no existe.");
		}else System.out.println(ErrMsg+"No hay base de datos seleccionada o ha sido eliminada.");
		return success;
	}
	
	public String alterTable(String id){ // TODO actions 
		return "";
	}
	
	/**
	 * Display tables of database in use
	 */
	public void showTables(){
		if(all.lbd.contains(BDU)){
			try{
				ObjectInputStream in = new ObjectInputStream(new FileInputStream(mbd.path+"/"+BDU+"/meta.data"));
				BD bd = (BD) in.readObject();
				in.close();
				if (bd.tablas.isEmpty())
					System.out.println("\nBase de datos "+BDU+" no tiene tablas.");
				else{
					System.out.println("\nLista de tablas almacenadas en base de datos "+BDU+".");
					for (int n=0;n<bd.tablas.size();n++){
						try{
							in = new ObjectInputStream(new FileInputStream(mbd.path+"/"+BDU+"/"+bd.tablas.get(n)));
							Tabla tb = (Tabla) in.readObject();
							in.close();
							System.out.println("\t- "+bd.tablas.get(n)+": "+tb.columnas.size()+" columna(s), " +
									tb.datos.get(0).size()+" registro(s), "+tb.foreingKeys.size()+" llave(s) " +
									"foranea(s).");
						}catch (Exception e) {}
					}
				}
			}catch (Exception e) {
				System.out.println("\nERROR FATAL: No se pudo mostrar las tablas.");
			}
		}
		else System.out.println("\nERROR: al mostrar tablas.\n\tNo hay base de datos seleccionada o ha sido eliminada.");
	}
	
	/**
	 * Display columns of a table
	 * @param tabla table selected
	 */
	public void showColumns(String tabla){
		if(all.lbd.contains(BDU)){
			try{
				ObjectInputStream in = new ObjectInputStream(new FileInputStream(mbd.path+"/"+BDU+"/meta.data"));
				BD bd = (BD) in.readObject();
				in.close();
				if (!bd.tablas.contains(tabla))
					System.out.println("\nBase de datos "+BDU+" no tiene la tabla "+tabla+".");
				else{
					System.out.println("\nLista de columnas de la tabla "+tabla+".");
					try{
						in = new ObjectInputStream(new FileInputStream(mbd.path+"/"+BDU+"/"+tabla));
						Tabla tb = (Tabla) in.readObject();
						in.close();
						for (int n=0; n<tb.columnas.size(); n++){
							System.out.print("\t- "+tb.columnas.get(n)+" : ");
							switch (tb.tipos.get(n)){
							case Tabla.INT:		System.out.print("INT\t"); break;
							case Tabla.FLOAT:	System.out.print("FLOAT\t"); break;
							case Tabla.DATE:	System.out.print("DATE\t"); break;
							default:			System.out.print("CHAR("+tb.tipos.get(n)+")"); break;
							} System.out.print("\t");
							if(tb.idchecks.get(n)!=null)
							switch (tb.checks.get(n).relation){
								case CHK.DISTINTO:		System.out.println("valor <> "+tb.checks.get(n).value); break;
								case CHK.MAYOR: 		System.out.println("valor > "+tb.checks.get(n).value); break;
								case CHK.MAYOR_IGUAL:	System.out.println("valor >= "+tb.checks.get(n).value); break;
								case CHK.MENOR:			System.out.println("valor < "+tb.checks.get(n).value); break;
								case CHK.MENOR_IGUAL:	System.out.println("valor <= "+tb.checks.get(n).value); break;
								case CHK.IGUAL:			System.out.println("valor = "+tb.checks.get(n).value); break;
							} else System.out.println("Sin check.");
						}
					}catch (Exception e) {}
				}
			}catch (Exception e) {
				System.out.println("\nERROR FATAL: No se pudo mostrar las columnas de la tabla "+tabla+".");
			}
		}else
		System.out.println("\nERROR: al mostrar columnas de tabla"+tabla+".\n\tNo hay base de datos seleccionada o ha sido eliminada.");
	}
	
	
	
	
	
	// DATA
	
	/**
	 * Insert values in a table 
	 * @param tabla table where store
	 * @param columnas columns selected
	 * @param data information to store
	 * @param type type of information
	 */
	public boolean insert(String tabla, LinkedList<String> columnas, LinkedList<Object> data, LinkedList<Integer> type){
		String ErrMsg = "\nERROR: INSERT INTO "+tabla+".\n\t";
		boolean success = false;
		if(all.lbd.contains(BDU)){
			BD bd = (BD) readObject(mbd.path+"/"+BDU+"/meta.data");
			if (bd.tablas.contains(tabla)){
				boolean error = false;
				Tabla tb = (Tabla) readObject(mbd.path+"/"+BDU+"/"+tabla);
				if(columnas.isEmpty()){
					int r = tb.insert(data, type);
					if(r!=-3) error = true;
					if(r==-2) System.out.println(ErrMsg+"El valor de la llave primaria "+tb.primaria+" ya existe.");
					else if (r==-1) System.out.println(ErrMsg+"Tabla "+tabla+" tiene "+tb.columnas.size()+" colu" +
							"mnas. Se ingresaron "+columnas.size()+" datos en la tupla.");
					else if(r>=0) System.out.println(ErrMsg+"Tipo de dato de columna "+tb.columnas.get(r)+" no es " +
							"el mismo que el ingresado.");
				}else{
					boolean columnas_validas = true;
					if (columnas.size()!=data.size()){
						columnas_validas = false;
						System.out.println(ErrMsg+"La cantidad de columnas es distinta al de elementos de la tupla.");
					}else for(int n=0; n<columnas.size(); n++){
						if(!tb.columnas.contains(columnas.get(n)))
							columnas_validas = false;
					}
					if(columnas_validas){
						LinkedList<Object> datap = new LinkedList<Object>();
						LinkedList<Integer> typep = new LinkedList<Integer>();
						for(int n=0; n<tb.columnas.size(); n++){
							if(columnas.contains(tb.columnas.get(n))){
								int index = columnas.indexOf(tb.columnas.get(n));
								datap.add(data.get(index));
								typep.add(type.get(index));
							}else{
								datap.add(null);
								typep.add(tb.tipos.get(n));
							}
						}
						int r = tb.insert(datap, typep);
						if (r!=-3) error = true;
						if(r==-2) System.out.println(ErrMsg+"El valor de la llave primaria "+tb.primaria+" ya existe" +
								" o es un valor nulo.");
					}
				}
				if(!error){
					saveTable(tabla, tb);
				}
			}else System.out.println(ErrMsg+"Base de datos "+BDU+" no tiene la tabla "+tabla+".");
		}else System.out.println(ErrMsg+"No hay base de datos seleccionada o ha sido eliminada.");
		return success;
	}
}
