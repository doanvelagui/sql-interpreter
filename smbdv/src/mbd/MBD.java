/*
 * Universidad del Valle de Guatemala
 * Administraci�n de la Informaci�n
 * 
 * Donald Vel�squez
 * carn�: 09379
 * abril - mayo de 2012
 */

package mbd;

import java.io.File;
import java.util.LinkedList;

import interpreteSQL.*;

public class MBD {

	// VARIABLES
	
	/** path of database storage **/
	protected String path;
	
	/** Configurations of BD storage **/
	protected CBD cbd = new CBD(this);
	
	/** Operations of BD storage **/
	private OBD obd = new OBD(this);
	
	/** Verbose flag **/
	protected boolean verbose = true;
	
	
	
	
	
	/// MAIN ///
	
	/**
	 * main
	 * @param args
	 */
	public static void main (String args[]){
		MBD manejador = new MBD();
		System.out.println("Para salir ingrese '\\salir'.\nPara ayuda ingrese '\\ayuda'.");
		boolean salir = false;
		java.util.Scanner in = new java.util.Scanner(System.in);
		while(!salir){
			System.out.print("\nIngreso: ");
			String ingreso = in.nextLine().trim();
			
			manejador.verbose = false;
			if (ingreso.endsWith("-v")){
				manejador.verbose = true;
				ingreso = ingreso.substring(0,ingreso.length()-2).trim();
			}
			
			if (ingreso.toLowerCase().equals("\\salir"))
				salir = true;
			else if (ingreso.toLowerCase().equals("\\ayuda"))
				System.out.println("\nIngrese el directorio completo donde se encuentra su script\nde SQL. " +
						"Si desea activar el modo verbose, agregar al final '-v'.\n\nPara conocer la ubicac" +
						"i�n de la base de datos ingrese '\\data'.\n\nSi desea conocer la gram�tica de algu" +
						"na instrucci�n SQL ingrese\n'\\gram <instruccion>'.\n");
			else if (ingreso.toLowerCase().equals("\\data"))
				System.out.println("\nBases de datos almacenadas en:\n\t"+manejador.path+"\n");
			else if (ingreso.toLowerCase().startsWith("\\gram"))
				System.out.println("\nEn construccion...\n\tRevisar la gram�tica definida en sqld.ATG.\n");
			else{
				File tmp = new File(ingreso);
				if(tmp.exists())
					manejador.interpretar(tmp.getAbsolutePath());
				else
					System.out.println("\tIngreso inv�lido.");
			}
		}
		manejador.cbd.close();
	}
	
		
	
	
	
	// METODOS
	
	/**
	 * Parsing a set of SQL instructions.
	 * @param path
	 */
	public void interpretar(String path){
		Scanner scanner = new Scanner(path);
    	Parser parser = new Parser(scanner, this);
    	parser.Parse();
	}
	
	
	
	// DATABASE
	
	/**
	 * Creates a database
	 * @param id identifier of database
	 */
	public void createDatabase(String id){
		if(obd.createDatabase(id) && verbose)
			System.out.println("\nBase de datos "+id+" creada.");
	}
	
	/**
	 * Drops a database
	 * @param id identifier of database
	 */
	public void dropDatabase(String id){
		if(obd.dropDatabase(id) && verbose)
			System.out.println("\nBase de datos "+id+" eliminada.");
	}
	
	/**
	 * Alter a database: rename
	 * @param id identifier of database
	 * @param newid new identifier of database
	 */
	public void alterDatabase(String id, String newid){
		if(obd.alterDatabase(id, newid) && verbose)
			System.out.println("\nBase de datos "+id+" renombrada a "+newid+".");
	}
	
	/**
	 * Display the names of all databases
	 */
	public void showDatabase(){
		obd.showDatabase();
	}
	
	/**
	 * Select a database to use
	 * @param id
	 */
	public void useDatabase(String id){
		if(obd.useDatabase(id) && verbose)
			System.out.println("\nBase de datos "+id+" en uso.");
	}
	
	
	
	
	
	// TABLE
	
	/**
	 * Create table
	 * @param id identifier of table
	 * @param columnas list of columns
	 * @param tipos list of types of columns
	 * @param idpk identifier of primary key
	 * @param pk column of primary key
	 * @param idfk list of identifiers of foreign keys
	 * @param cfk columns of foreign keys
	 * @param tfk tables referenced by foreign keys
	 */
	public void createTable(String id, LinkedList<String> columnas, LinkedList<Integer> tipos, String idpk, String pk,
			LinkedList<String> idfk, LinkedList<String> cfk, LinkedList<String> tfk, LinkedList<String> idchk,
			LinkedList<String> colchk, LinkedList<Integer> relchk, LinkedList<String> valchk, LinkedList<Integer> tipchk){
		if (obd.createTable(id, columnas, tipos, idpk, pk, idfk, cfk, tfk, idchk, colchk, relchk, valchk, tipchk) && verbose)
			System.out.println("\nTabla "+id+" creada.");
	}
	
	/**
	 * Drop a table
	 * @param id identifier of table
	 */
	public void dropTable(String id){
		if (obd.dropTable(id) && verbose)
			System.out.println("\nTabla "+id+" eliminada.");
	}
	
	/**
	 * Rename a table
	 * @param id table to rename
	 * @param newid new name of table
	 */
	public void alterTable(String id, String newid){
		if(obd.alterTable(id, newid) && verbose)
			System.out.println("\nTabla"+id+" renombrada a "+newid+".");
	}
	
	public void alterTable(String pk){ 
		System.out.println("DDDD");
		// TODO 
	}
	
	/**
	 * Display tables of database in use
	 */
	public void showTables(){
		obd.showTables();
	}
	
	/**
	 * Display columns of a table
	 * @param tabla table selected
	 */
	public void showColumns(String tabla){
		obd.showColumns(tabla);
	}
	
	
	
	
	
	// DATA
	
	/**
	 * Insert values in a table 
	 * @param tabla table where store
	 * @param columnas columns selected
	 * @param data information to store
	 * @param type type of information
	 */
	public void insert(String tabla, LinkedList<String> columnas, LinkedList<Object> data, LinkedList<Integer> type){
		if(obd.insert(tabla, columnas, data, type) && verbose) 
			System.out.println("\nDato almacenados en tabla "+tabla);
	}
}
