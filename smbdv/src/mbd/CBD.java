/*
 * Universidad del Valle de Guatemala
 * Administraci�n de la Informaci�n
 * 
 * Donald Vel�squez
 * carn�: 09379
 * abril - mayo de 2012
 */

package mbd;

import java.io.*;

/**
 * Load the configurations of database
 * @author doanvelagui
 */
public class CBD {
	
	// CONSTANTES
	
	/** path of configuration file **/
	private final static String config = "./config/cbd";
	
	
	
	// VARIABLES
	
	/** load configurations flag <br>-1: Not load<br>0: Loaded<br>1: Set path**/
	protected int load_cbd;
	
	/** main **/
	protected MBD mbd;
	
	
	// CONSTRUCTOR
	
	/**
	 * Create CBD <br>
	 * This load the state of database storage.
	 */
	public CBD(MBD mbd){
		System.out.println("Cargando configuraciones...");
		load_cbd = -1;
		this.mbd = mbd;
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(config)));
			mbd.path = br.readLine();
			
			File tmp = new File(mbd.path);
			if(!tmp.exists()){ 
				System.out.println("El directorio '"+mbd.path+"' no exite.\n\tRevisar el archivo de configuraci�n 'CBD'.");
				close();	// Panic mode... TODO pedir nuevo path?
			} else{
				System.out.println("Configuraciones cargadas.\n");
				load_cbd = 0;
			}br.close();
		}
		
		catch (FileNotFoundException e) {
			System.out.println("ERROR: No se ha definido directorio para almacenamiento de las bases de datos\n");
			mbd.path = selectPath("Por favor ingrese el directorio o ingrese 'salir' para salir: ");
			load_cbd = 1;
		}
		catch (Exception e) {
			System.out.println("ERROR: La configuraci�n no contenida no es correcta\n");
			mbd.path = selectPath("Por favor ingrese el directorio o ingrese 'salir' para salir: ");
			load_cbd = 1;
		}
		//*DEBUG:*/ System.out.println(path); System.out.println(load_cbd);
	}
	
	
	
	
	// METODOS
	
	/**
	 * Select a new path
	 * @param message message to display
	 * @return String path
	 */
	private String selectPath(String message){
		System.out.print(message);
		java.util.Scanner in = new java.util.Scanner(System.in);
		
		File tmp = new File(in.next());
		while(!tmp.exists()){
			if (tmp.getPath().equals("salir"))
				close();
			System.out.print("Ingreso inv�lido, intente de nuevo: ");
			tmp = new File(in.next());
		}
		
		return tmp.getAbsolutePath();
	}
	
	/**
	 * Stop the execution.
	 */
	public void close(){
		if(load_cbd == 1)
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter(config));
				bw.write(mbd.path);
				bw.close();
			} catch (IOException e) {
				System.err.println("ERROR: Las configuraciones no se pudieron guardar.");
			}
		System.exit(0);
	}
}
