

package interpreteSQL;

import mbd.*;
import db.*;
import db.Tabla.CHK;
import java.util.*;



public class Parser {
	public static final int _EOF = 0;
	public static final int _id = 1;
	public static final int _number = 2;
	public static final int _float = 3;
	public static final int _date = 4;
	public static final int _string = 5;
	public static final int maxT = 52;

	static final boolean T = true;
	static final boolean x = false;
	static final int minErrDist = 2;

	public Token t;    // last recognized token
	public Token la;   // lookahead token
	int errDist = minErrDist;
	
	public Scanner scanner;
	public Errors errors;
	
	private MBD mbd;

	

	public Parser(Scanner scanner, MBD mbd) {
		this.scanner = scanner;
		this.mbd = mbd;
		errors = new Errors();
	}

	void SynErr (int n) {
		if (errDist >= minErrDist) errors.SynErr(la.line, la.col, n);
		errDist = 0;
	}

	public void SemErr (String msg) {
		if (errDist >= minErrDist) errors.SemErr(t.line, t.col, msg);
		errDist = 0;
	}
	
	void Get () {
		for (;;) {
			t = la;
			la = scanner.Scan();
			if (la.kind <= maxT) {
				++errDist;
				break;
			}

			la = t;
		}
	}
	
	void Expect (int n) {
		if (la.kind==n) Get(); else { SynErr(n); }
	}
	
	boolean StartOf (int s) {
		return set[s][la.kind];
	}
	
	void ExpectWeak (int n, int follow) {
		if (la.kind == n) Get();
		else {
			SynErr(n);
			while (!StartOf(follow)) Get();
		}
	}
	
	boolean WeakSeparator (int n, int syFol, int repFol) {
		int kind = la.kind;
		if (kind == n) { Get(); return true; }
		else if (StartOf(repFol)) return false;
		else {
			SynErr(n);
			while (!(set[syFol][kind] || set[repFol][kind] || set[0][kind])) {
				Get();
				kind = la.kind;
			}
			return StartOf(syFol);
		}
	}
	
	void SQLD() {
		while (StartOf(1)) {
			switch (la.kind) {
			case 6: {
				create();
				break;
			}
			case 28: {
				drop();
				break;
			}
			case 29: {
				alter();
				break;
			}
			case 34: {
				show();
				break;
			}
			case 39: {
				use();
				break;
			}
			case 40: {
				insert();
				break;
			}
			case 43: {
				update();
				break;
			}
			case 47: {
				delete();
				break;
			}
			case 48: {
				select();
				break;
			}
			}
		}
	}

	void create() {
		Expect(6);
		String namedb = ""; 
							  	String nametb = "";
		LinkedList<String> columnas = new LinkedList<String>();
		LinkedList<Integer> tipos = new LinkedList<Integer>();
							  	String idpk = "";
		String pk = "";
							  	LinkedList<String> idfk = new LinkedList<String>();
		LinkedList<String> cfk = new LinkedList<String>();
		LinkedList<String> tfk = new LinkedList<String>();
							  	LinkedList<String> idchk = new LinkedList<String>();
		LinkedList<String> colchk = new LinkedList<String>();
		LinkedList<Integer> relchk = new LinkedList<Integer>();
		LinkedList<String> valchk = new LinkedList<String>();
		LinkedList<Integer> tipchk = new LinkedList<Integer>();
		
		if (la.kind == 7) {
			Get();
			Expect(1);
			namedb = t.val; 
		} else if (la.kind == 8) {
			Get();
			Expect(1);
			nametb = t.val; 
			Expect(9);
			Expect(1);
			columnas.add(t.val);	
			if (la.kind == 10) {
				Get();
				tipos.add(Tabla.INT);	
			} else if (la.kind == 11) {
				Get();
				tipos.add(Tabla.FLOAT);	
			} else if (la.kind == 12) {
				Get();
				Expect(9);
				Expect(2);
				tipos.add(Integer.parseInt(t.val));	
				Expect(13);
			} else if (la.kind == 14) {
				Get();
				tipos.add(Tabla.DATE);	
			} else SynErr(53);
			while (la.kind == 15) {
				Get();
				if (la.kind == 1) {
					Get();
					columnas.add(t.val);	
					if (la.kind == 10) {
						Get();
						tipos.add(Tabla.INT);	
					} else if (la.kind == 11) {
						Get();
						tipos.add(Tabla.FLOAT);	
					} else if (la.kind == 12) {
						Get();
						Expect(9);
						Expect(2);
						tipos.add(Integer.parseInt(t.val));	
						Expect(13);
					} else if (la.kind == 14) {
						Get();
						tipos.add(Tabla.DATE);	
					} else SynErr(54);
				} else if (la.kind == 16) {
					Get();
					Expect(17);
					Expect(1);
					idpk = t.val;	
					Expect(9);
					Expect(1);
					pk = t.val;		
					Expect(13);
				} else if (la.kind == 18) {
					Get();
					Expect(17);
					Expect(1);
					idfk.add(t.val);
					Expect(9);
					Expect(1);
					cfk.add(t.val);	
					Expect(13);
					Expect(19);
					Expect(1);
					tfk.add(t.val);	
				} else if (la.kind == 20) {
					Get();
					Expect(1);
					idchk.add(t.val);	
					Expect(9);
					Expect(1);
					colchk.add(t.val);	
					switch (la.kind) {
					case 21: {
						Get();
						relchk.add(CHK.MENOR);	
						break;
					}
					case 22: {
						Get();
						relchk.add(CHK.MENOR_IGUAL);	
						break;
					}
					case 23: {
						Get();
						relchk.add(CHK.IGUAL);	
						break;
					}
					case 24: {
						Get();
						relchk.add(CHK.MAYOR);	
						break;
					}
					case 25: {
						Get();
						relchk.add(CHK.MAYOR_IGUAL);	
						break;
					}
					case 26: {
						Get();
						relchk.add(CHK.DISTINTO);	
						break;
					}
					default: SynErr(55); break;
					}
					if (la.kind == 2) {
						Get();
						tipchk.add(Tabla.INT);	
					} else if (la.kind == 3) {
						Get();
						tipchk.add(Tabla.FLOAT);
					} else SynErr(56);
					valchk.add(t.val);	
					Expect(13);
				} else SynErr(57);
			}
			Expect(13);
		} else SynErr(58);
		Expect(27);
		if (!namedb.equals("") && errors.count==0) 
		mbd.createDatabase(namedb);
		if (!nametb.equals("") && errors.count==0)
		mbd.createTable(nametb, columnas, tipos, 
		idpk, pk, idfk, cfk, tfk, idchk, colchk,
		relchk, valchk, tipchk);
		
	}

	void drop() {
		Expect(28);
		String namedb = ""; 
		String nametb = "";
		
		if (la.kind == 7) {
			Get();
			Expect(1);
			namedb = t.val; 
		} else if (la.kind == 8) {
			Get();
			Expect(1);
			nametb = t.val; 
		} else SynErr(59);
		Expect(27);
		if (!namedb.equals("") && errors.count==0) 
		mbd.dropDatabase(namedb);
		if (!nametb.equals("") && errors.count==0)
		mbd.dropTable(nametb);
		
	}

	void alter() {
		Expect(29);
		String namedb = ""; 
		String newNamedb = "";
		String nametb = "";
		String newNametb = "";
								  	LinkedList<String> addColn = new LinkedList<String>();
		LinkedList<Integer> addColt = new LinkedList<Integer>();
		LinkedList<String> delCol = new LinkedList<String>();
									
		if (la.kind == 7) {
			Get();
			Expect(1);
			namedb = t.val;	
			Expect(30);
			Expect(31);
			Expect(1);
			newNamedb = t.val;	
		} else if (la.kind == 8) {
			Get();
			Expect(1);
			nametb = t.val; 
			if (la.kind == 30) {
				Get();
				Expect(31);
				Expect(1);
				newNametb = t.val;	
			} else if (la.kind == 9) {
				Get();
				if (la.kind == 32) {
					Get();
					if (la.kind == 33) {
						Get();
						Expect(1);
						addColn.add(t.val);	
						if (la.kind == 10) {
							Get();
							addColt.add(Tabla.INT);	
						} else if (la.kind == 11) {
							Get();
							addColt.add(Tabla.FLOAT);	
						} else if (la.kind == 12) {
							Get();
							Expect(9);
							Expect(2);
							addColt.add(Integer.parseInt(t.val));	
							Expect(13);
						} else if (la.kind == 14) {
							Get();
							addColt.add(Tabla.DATE);	
						} else SynErr(60);
					} else if (la.kind == 16) {
						Get();
						Expect(17);
						Expect(1);
						Expect(9);
						Expect(1);
						Expect(13);
					} else if (la.kind == 18) {
						Get();
						Expect(17);
						Expect(1);
						Expect(9);
						Expect(1);
						Expect(13);
						Expect(19);
						Expect(1);
					} else if (la.kind == 20) {
						Get();
						Expect(1);
						Expect(9);
						Expect(1);
						switch (la.kind) {
						case 21: {
							Get();
							break;
						}
						case 22: {
							Get();
							break;
						}
						case 23: {
							Get();
							break;
						}
						case 24: {
							Get();
							break;
						}
						case 25: {
							Get();
							break;
						}
						case 26: {
							Get();
							break;
						}
						default: SynErr(61); break;
						}
						if (la.kind == 2) {
							Get();
						} else if (la.kind == 3) {
							Get();
						} else SynErr(62);
						Expect(13);
					} else SynErr(63);
				} else if (la.kind == 28) {
					Get();
					if (la.kind == 33) {
						Get();
						Expect(1);
						delCol.add(t.val);	
					} else if (la.kind == 16) {
						Get();
						Expect(17);
						Expect(1);
					} else if (la.kind == 18) {
						Get();
						Expect(17);
						Expect(1);
					} else if (la.kind == 20) {
						Get();
						Expect(1);
					} else SynErr(64);
				} else SynErr(65);
				Expect(13);
			} else SynErr(66);
		} else SynErr(67);
		Expect(27);
		if (!namedb.equals("")&&errors.count==0&&!newNamedb.equals(""))
		mbd.alterDatabase(namedb, newNamedb);
		if (!nametb.equals("")&&errors.count==0&&!newNametb.equals(""))
		mbd.alterTable(nametb,newNametb);
		if (!nametb.equals("")&&errors.count==0&&newNametb.equals(""))
		mbd.alterTable(nametb);
		
	}

	void show() {
		Expect(34);
		int tipoShow = 0;
		String tabla = "";
		
		if (la.kind == 35) {
			Get();
			tipoShow = 1;	
		} else if (la.kind == 36) {
			Get();
			Expect(37);
			tipoShow = 2;	
			Expect(1);
			tabla = t.val;	
		} else if (la.kind == 38) {
			Get();
			tipoShow = 3;	
		} else SynErr(68);
		Expect(27);
		if (tipoShow==1 && errors.count==0)
		mbd.showTables();
		if (tipoShow==2 && errors.count==0)
		mbd.showColumns(tabla);
		if (tipoShow==3 && errors.count==0)
		mbd.showDatabase();
		
	}

	void use() {
		Expect(39);
		Expect(7);
		Expect(1);
		String namedb = t.val;	
		Expect(27);
		if (errors.count==0)
		mbd.useDatabase(namedb);
		
	}

	void insert() {
		Expect(40);
		Expect(41);
		String tabla = "";
		LinkedList<String> columnas = new LinkedList<String>();
		LinkedList<Object> data = new LinkedList<Object>();
		LinkedList<Integer> type = new LinkedList<Integer>();
		
		Expect(1);
		tabla = t.val;	
		if (la.kind == 9) {
			Get();
			Expect(1);
			columnas.add(t.val);
			while (la.kind == 15) {
				Get();
				Expect(1);
				columnas.add(t.val);
			}
			Expect(13);
		}
		Expect(42);
		Expect(9);
		if (la.kind == 2) {
			Get();
			type.add(Tabla.INT);	
			data.add(Integer.parseInt(t.val));
			
		} else if (la.kind == 3) {
			Get();
			type.add(Tabla.FLOAT);
			data.add(Float.parseFloat(t.val));
			
		} else if (la.kind == 4) {
			Get();
			type.add(Tabla.DATE);
			data.add(t.val);
			
		} else if (la.kind == 5) {
			Get();
			type.add(t.val.length());
			data.add(t.val);
			
		} else SynErr(69);
		while (la.kind == 15) {
			Get();
			if (la.kind == 2) {
				Get();
				type.add(Tabla.INT);	
				data.add(Integer.parseInt(t.val));
				
			} else if (la.kind == 3) {
				Get();
				type.add(Tabla.FLOAT);
				data.add(Float.parseFloat(t.val));
				
			} else if (la.kind == 4) {
				Get();
				type.add(Tabla.DATE);
				data.add(t.val);
				
			} else if (la.kind == 5) {
				Get();
				type.add(t.val.length());
				data.add(t.val);
				
			} else SynErr(70);
		}
		Expect(13);
		Expect(27);
		if (errors.count==0)
		mbd.insert(tabla, columnas, data, type);
		
	}

	void update() {
		Expect(43);
		Expect(1);
		Expect(44);
		Expect(1);
		Expect(23);
		if (StartOf(2)) {
			value();
		} else if (la.kind == 45) {
			Get();
		} else SynErr(71);
		while (la.kind == 15) {
			Get();
			Expect(1);
			Expect(23);
			if (StartOf(2)) {
				value();
			} else if (la.kind == 45) {
				Get();
			} else SynErr(72);
		}
		if (la.kind == 46) {
			Get();
		}
		Expect(27);
	}

	void delete() {
		Expect(47);
		Expect(37);
		Expect(1);
		if (la.kind == 46) {
			Get();
		}
		Expect(27);
	}

	void select() {
		Expect(48);
		if (la.kind == 49) {
			Get();
		} else if (la.kind == 1) {
			Get();
			while (la.kind == 15) {
				Get();
				Expect(1);
			}
		} else SynErr(73);
		if (la.kind == 37) {
			Get();
			Expect(1);
		}
		if (la.kind == 46) {
			Get();
		}
		if (la.kind == 50) {
			Get();
			Expect(51);
		}
		Expect(27);
	}

	void value() {
		if (la.kind == 2) {
			Get();
		} else if (la.kind == 3) {
			Get();
		} else if (la.kind == 4) {
			Get();
		} else if (la.kind == 5) {
			Get();
		} else SynErr(74);
	}



	public void Parse() {
		la = new Token();
		la.val = "";		
		Get();
		SQLD();
		Expect(0);

	}

	private static final boolean[][] set = {
		{T,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x},
		{x,x,x,x, x,x,T,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, T,T,x,x, x,x,T,x, x,x,x,T, T,x,x,T, x,x,x,T, T,x,x,x, x,x},
		{x,x,T,T, T,T,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x}

	};
} // end Parser


class Errors {
	public int count = 0;                                    // number of errors detected
	public java.io.PrintStream errorStream = System.out;     // error messages go to this stream
	public String errMsgFormat = "ERROR: linea {0} columna {1}: {2}"; // 0=line, 1=column, 2=text
	
	protected void printMsg(int line, int column, String msg) {
		StringBuffer b = new StringBuffer(errMsgFormat);
		int pos = b.indexOf("{0}");
		if (pos >= 0) { b.delete(pos, pos+3); b.insert(pos, line); }
		pos = b.indexOf("{1}");
		if (pos >= 0) { b.delete(pos, pos+3); b.insert(pos, column); }
		pos = b.indexOf("{2}");
		if (pos >= 0) b.replace(pos, pos+3, msg);
		errorStream.println(b.toString());
	}
	
	public void SynErr (int line, int col, int n) {
		String s;
		switch (n) {
			case 0: s = "EOF expected"; break;
			case 1: s = "id expected"; break;
			case 2: s = "number expected"; break;
			case 3: s = "float expected"; break;
			case 4: s = "date expected"; break;
			case 5: s = "string expected"; break;
			case 6: s = "\"create\" expected"; break;
			case 7: s = "\"database\" expected"; break;
			case 8: s = "\"table\" expected"; break;
			case 9: s = "\"(\" expected"; break;
			case 10: s = "\"int\" expected"; break;
			case 11: s = "\"float\" expected"; break;
			case 12: s = "\"char\" expected"; break;
			case 13: s = "\")\" expected"; break;
			case 14: s = "\"date\" expected"; break;
			case 15: s = "\",\" expected"; break;
			case 16: s = "\"primary\" expected"; break;
			case 17: s = "\"key\" expected"; break;
			case 18: s = "\"foreign\" expected"; break;
			case 19: s = "\"references\" expected"; break;
			case 20: s = "\"check\" expected"; break;
			case 21: s = "\"<\" expected"; break;
			case 22: s = "\"<=\" expected"; break;
			case 23: s = "\"=\" expected"; break;
			case 24: s = "\">\" expected"; break;
			case 25: s = "\">=\" expected"; break;
			case 26: s = "\"<>\" expected"; break;
			case 27: s = "\";\" expected"; break;
			case 28: s = "\"drop\" expected"; break;
			case 29: s = "\"alter\" expected"; break;
			case 30: s = "\"rename\" expected"; break;
			case 31: s = "\"to\" expected"; break;
			case 32: s = "\"add\" expected"; break;
			case 33: s = "\"column\" expected"; break;
			case 34: s = "\"show\" expected"; break;
			case 35: s = "\"tables\" expected"; break;
			case 36: s = "\"columns\" expected"; break;
			case 37: s = "\"from\" expected"; break;
			case 38: s = "\"databases\" expected"; break;
			case 39: s = "\"use\" expected"; break;
			case 40: s = "\"insert\" expected"; break;
			case 41: s = "\"into\" expected"; break;
			case 42: s = "\"values\" expected"; break;
			case 43: s = "\"update\" expected"; break;
			case 44: s = "\"set\" expected"; break;
			case 45: s = "\"default\" expected"; break;
			case 46: s = "\"where\" expected"; break;
			case 47: s = "\"delete\" expected"; break;
			case 48: s = "\"select\" expected"; break;
			case 49: s = "\"*\" expected"; break;
			case 50: s = "\"order\" expected"; break;
			case 51: s = "\"by\" expected"; break;
			case 52: s = "??? expected"; break;
			case 53: s = "invalid create"; break;
			case 54: s = "invalid create"; break;
			case 55: s = "invalid create"; break;
			case 56: s = "invalid create"; break;
			case 57: s = "invalid create"; break;
			case 58: s = "invalid create"; break;
			case 59: s = "invalid drop"; break;
			case 60: s = "invalid alter"; break;
			case 61: s = "invalid alter"; break;
			case 62: s = "invalid alter"; break;
			case 63: s = "invalid alter"; break;
			case 64: s = "invalid alter"; break;
			case 65: s = "invalid alter"; break;
			case 66: s = "invalid alter"; break;
			case 67: s = "invalid alter"; break;
			case 68: s = "invalid show"; break;
			case 69: s = "invalid insert"; break;
			case 70: s = "invalid insert"; break;
			case 71: s = "invalid update"; break;
			case 72: s = "invalid update"; break;
			case 73: s = "invalid select"; break;
			case 74: s = "invalid value"; break;
			default: s = "error " + n; break;
		}
		printMsg(line, col, s);
		count++;
	}

	public void SemErr (int line, int col, String s) {	
		printMsg(line, col, s);
		count++;
	}
	
	public void SemErr (String s) {
		errorStream.println(s);
		count++;
	}
	
	public void Warning (int line, int col, String s) {	
		printMsg(line, col, s);
	}
	
	public void Warning (String s) {
		errorStream.println(s);
	}
} // Errors


class FatalError extends RuntimeException {
	public static final long serialVersionUID = 1L;
	public FatalError(String s) { super(s); }
}
