/*
 * Universidad del Valle de Guatemala
 * Administraci�n de la Informaci�n
 * 
 * Donald Vel�squez
 * carn�: 09379
 * abril - mayo de 2012
 */

package db;

import java.util.*;
import java.io.Serializable;

/**
 * Represent a table of a data base
 * @author doanvelagui
 */
public class Tabla implements Serializable{
	
	// CONSTANTES
	
	/** Constant value for serialization **/
	private static final long serialVersionUID = 3626;
	
	/** Type integer **/
	public final static int INT		= -1;
	
	/** Type float **/
	public final static int FLOAT	= -2;
	
	/** Type date **/
	public final static int DATE	= -3;
	
	
	
	
	
	// VARIABLES
	
	/** Columns of table **/
	public LinkedList<String> columnas;
	
	/** Type of columns **/
	public LinkedList<Integer> tipos;
	
	
	/** Identifier of checks **/
	public LinkedList<String> idchecks;
	
	/** List of checks **/
	public LinkedList<CHK> checks;
	
	
	/** Data **/
	public LinkedList<LinkedList<Object>> datos;
	
	
	/** Identifier of Primary Key **/
	public String idPrimaria;
	
	/** Primary Key **/
	public String primaria;
	
	
	/** List of foreign keys of table **/
	public HashMap<String, FKR> foreingKeys;
	
	/** List of foreign keys who references this table **/
	public HashMap<String, LinkedList<String>> references;
	
	
	
	
	
	// CONSTRUCTOR
	
	/**
	 * Create a table
	 */
	public Tabla (){
		columnas = new LinkedList<String>();
		tipos = new LinkedList<Integer>();
		
		idchecks = new LinkedList<String>();
		checks = new LinkedList<Tabla.CHK>();
		
		datos = new LinkedList<LinkedList<Object>>();
		
		idPrimaria = "";
		primaria = "";
		
		foreingKeys = new HashMap<String, Tabla.FKR>();
		references = new  HashMap<String, LinkedList<String>>();
	}
	
	
	
	
	
	// DATA DEFINITION
		
	/**
	 * Adds a column to table
	 * @param columna to add
	 * @return boolean value
	 */
	public boolean addColumn(String columna, Integer tipo){
		return columnas.add(columna) && tipos.add(tipo) && datos.add(new LinkedList<Object>()) && idchecks.add(null) && checks.add(null);
	}
	
	/**
	 * Remove a column from table
	 * @param column to remove
	 * @return boolean value
	 */
	public boolean removeColumn(String columna){
		if(columnas.contains(columna)){
			int index = columnas.indexOf(columna);
			columnas.remove(index); tipos.remove(index); datos.remove(index); idchecks.remove(index); checks.remove(index);
			return true;
		}else return false;
	}
	
	/**
	 * Add primary key if not defined
	 * @param pk identifier of column
	 * @param idpk identifier of primary key
	 * @return true if success
	 */
	public boolean addPrimaryKey(String pk, String idpk){
		primaria = pk;
		idPrimaria = idpk;
		return true;
	}
	
	/**
	 * Removes primary key
	 * @param idpk identifier of primary key
	 * @return true if removed
	 */
	public boolean removePrimaryKey(String idpk){
		if(idPrimaria.equals(idpk)){
			primaria = "";
			idPrimaria = "";
			return true;
		}else return false;
	}
	
	/**
	 * Add a foreign key
	 * @param columna column of this table
	 * @param tabla referenced table
	 * @param idfk identifier of Primary Key
	 * @return
	 */
	public boolean addForeignKey(String columna, String tabla, String idfk){
		if(!foreingKeys.containsKey(idfk)){
			foreingKeys.put(idfk, new FKR(columna, tabla));
			return true;
		}else return true;
	}
	
	/**
	 * Remove a foreign key
	 * @param idfk identifier of foreign key
	 * @return true if removed
	 */
	public boolean removeForeignKey(String idfk){
		if(foreingKeys.containsKey(idfk)){
			foreingKeys.remove(idfk);
			return true;
		}else return false;
	}
	
	/**
	 * Add tables who references this
	 * @param tabla table who references
	 * @param idpk name of reference
	 * @return true if success
	 */
	public boolean addReference(String tabla, String idpk){
		if (references.containsKey(tabla)){
			if(!references.get(tabla).contains(idpk)){
				references.get(tabla).add(idpk);
				return true;
			} else return false;
		} else {
			LinkedList<String> tmp = new LinkedList<String>();
			tmp.add(idpk);
			references.put(tabla, tmp);
			return true;
		}
	}
	
	/**
	 * Remove a reference to this table
	 * @param tabla table who references
	 * @param idpk name of reference
	 * @return true if success
	 */
	public boolean removeReference(String tabla, String idpk){
		if (references.containsKey(tabla)){
			if(references.get(tabla).contains(idpk))
				return references.get(tabla).contains(idpk);
		}return false;
		
	}
	
	/**
	 * Add check to column 
	 * @param column column associated
	 * @param check check associated
	 * @return true if success
	 */
	public boolean addCheck(String column, String idchk, int relchk, Object valchk){
		if(columnas.contains(column)){
			int index = columnas.indexOf(column);
			if(idchecks.get(index)!=null)
				System.out.println("\nNOTA: check "+idchecks.get(index)+" sobreescrito por check "+idchk+".");
			idchecks.set(index, idchk);
			checks.set(index, new CHK(relchk, valchk));
			return true;
		}return false;
	}
	
	/**
	 * Removes a check
	 * @param idchk identifier of check
	 * @return
	 */
	public boolean removeCheck(String idchk){
		if(idchecks.contains(idchk)){
			int index = idchecks.indexOf(idchk);
			idchecks.set(index, null);
			checks.set(index, null);
		}return false;
	}
	
	
	
	
	
	// DATA MANAGE
	
	/**
	 * Insert values in table
	 * @param data values to store
	 * @param type type of value
	 * @return -3 if success.<BR>-2 if primary key is duplicated or is null<BR>-1 if number of 
	 * 		columns aren't equal<BR> 'n' if column 'n' aren't same type than data to store.<BR>
	 * 		-4 if check is violated.
	 */
	public int insert(LinkedList<Object> data, LinkedList<Integer> type){
		if (data.size()==columnas.size() && type.size()==columnas.size()){
			for (int n=0; n<columnas.size(); n++){
				if(primaria.equals(columnas.get(n))){
					if(datos.get(n).contains(data.get(n)))
						return -2;
					if(data.get(n)==null)
						return -2;
				}
				
				/* ERROR AL REVISAR CHECKS
				if(idchecks.get(n)!=null && type.get(n).equals(INT)){
					Integer val = (Integer) data.get(n);
					switch (checks.get(n).relation){
						case CHK.DISTINTO:
							if(val.equals((Integer)checks.get(n).value)) return -4-n; break;
						case CHK.MAYOR:
							if(val<=(Integer)checks.get(n).value) return -4-n; break;
						case CHK.MAYOR_IGUAL:
							if(val<(Integer)checks.get(n).value) return -4-n; break;
						case CHK.MENOR:
							if(val>=(Integer)checks.get(n).value) return -4-n; break;
						case CHK.MENOR_IGUAL:
							if(val>(Integer)checks.get(n).value) return -4-n; break;
						case CHK.IGUAL:
							if(!val.equals((Integer)checks.get(n).value)) return -4-n; break;
					}
				}else if(idchecks.get(n)!=null && type.get(n).equals(FLOAT)){
					Float val = (Float) data.get(n);
					switch (checks.get(n).relation){
						case CHK.DISTINTO:
							if(val.equals((Float)checks.get(n).value)) return -4-n; break;
						case CHK.MAYOR:
							if(val<=(Float)checks.get(n).value) return -4-n; break;
						case CHK.MAYOR_IGUAL:
							if(val<(Float)checks.get(n).value) return -4-n; break;
						case CHK.MENOR:
							if(val>=(Float)checks.get(n).value) return -4-n; break;
						case CHK.MENOR_IGUAL:
							if(val>(Float)checks.get(n).value) return -4-n; break;
						case CHK.IGUAL:
							if(!val.equals((Float)checks.get(n).value)) return -4-n; break;
					}
				} //*/
				
				if(type.get(n)<0 && type.get(n).equals(tipos.get(n)))
					datos.get(n).add(data.get(n));
				else if(type.get(n)>0 && type.get(n)<=tipos.get(n))
					datos.get(n).add(data.get(n));
				else return n;
			}
		}else return -1;
		return -3;
	}
	
	
	
	
	
	// CLASSES
	
	/**
	 * Foreign Key Reference
	 * @author doanvelagui
	 */
	public class FKR implements Serializable{
		// CONSTANTES
		
		/** Constant value for serialization **/
		private static final long serialVersionUID = 3626;
		
		// VARIABLES
		
		/** Column used to FK **/
		public String column;
		
		/** Table referenced by FK **/
		public String table;
		
		// CONSTRUCTOR
		
		/**
		 * Foreign Key Reference
		 * @param column
		 * @param table
		 */
		public FKR(String column, String table){
			this.column=column;
			this.table=table;
		}
	}
	
	/**
	 * Checks
	 * @author doanvelagui
	 */
	public class CHK implements Serializable{

		// CONSTANTES
		
		/** Constant value for serialization **/
		private static final long serialVersionUID = 3626;
		
		/** Kind of CHK **/
		public final static int MAYOR = 1;
		public final static int MAYOR_IGUAL = 2;
		public final static int IGUAL = 3;
		public final static int MENOR = 4;
		public final static int MENOR_IGUAL = 5;
		public final static int DISTINTO = 6;
		
		// VARIABLES
		
		/** kind of check **/
		public int relation;
		
		/** Value of relation**/
		public Object value;
		
		// CONSTRUCTOR
		
		/**
		 * Kind of relation and value
		 * @param relation
		 * @param value
		 */
		public CHK(int relation, Object value){
			this.relation=relation;
			this.value=value;
		}
	}
}
