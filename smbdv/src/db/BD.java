/*
 * Universidad del Valle de Guatemala
 * Administraci�n de la Informaci�n
 * 
 * Donald Vel�squez
 * carn�: 09379
 * abril - mayo de 2012
 */

package db;

import java.util.*;
import java.io.Serializable;

/**
 * Represent a database
 * @author doanvelagui
 */
public class BD implements Serializable{

	// CONSTANTES
	
	/** Constant value for serialization **/
	private static final long serialVersionUID = 3626;

	
	
	// VARIABLES
	
	/** List of Tables **/
	public LinkedList<String> tablas;
	
	
	
	// CONSTRUCTOR
	
	/**
	 * Create a object
	 * @param path
	 * @param id
	 */
	public BD (){
		tablas = new LinkedList<String>();
	}
	
	
	
	// METODOS
	
	/**
	 * Adds a table to data base
	 * @param tabla to add
	 * @return boolean value
	 */
	public boolean addTable(String tabla){
		return tablas.add(tabla);
	}
	
	/**
	 * Remove a table from data base
	 * @param tabla to remove
	 * @return boolean value
	 */
	public boolean removeTable(String tabla){
		return tablas.remove(tabla);
	}
	
	/**
	 * Set the name of a database
	 * @param bd name of database
	 * @param newbd 
	 * @return
	 */
	public boolean setTable(String bd, String newbd){
		if (tablas.indexOf(bd)!= -1)
			return (bd.equals(tablas.set(tablas.indexOf(bd), newbd)));
		else return false;
	}
}
