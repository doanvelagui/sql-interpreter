/*
 * Universidad del Valle de Guatemala
 * Administraci�n de la Informaci�n
 * 
 * Donald Vel�squez
 * carn�: 09379
 * abril - mayo de 2012
 */

package db;

import java.util.*;
import java.io.Serializable;

/**
 * Is a list of all databases
 * @author doanvelagui
 */
public class ABD implements Serializable{

	// CONSTANTES

	/** Constant value for serialization **/
	private static final long serialVersionUID = 3626;

		
		
	// VARIABLES
	
	/** List of databases **/
	public LinkedList<String> lbd;
	
	
	
	// CONSTRUCTOR
	
	/**
	 * Create a ABD
	 * @param path
	 */
	public ABD(){
		lbd = new LinkedList<String>();
	}
	
	
	
	// METODOS
	
	/**
	 * Add a name of database
	 * @param bd name of database
	 * @return true if added
	 */
	public boolean addBD(String bd){
		return lbd.add(bd);
	}
	
	/**
	 * Removes the name of a database
	 * @param bd name of database
	 * @return true if removed
	 */
	public boolean removeBD(String bd){
		return lbd.remove(bd);
	}
	
	/**
	 * Set the name of a database
	 * @param bd name of database
	 * @param newbd 
	 * @return
	 */
	public boolean setBD(String bd, String newbd){
		if (lbd.indexOf(bd)!= -1)
			return (bd.equals(lbd.set(lbd.indexOf(bd), newbd)));
		else return false;
	}
}
